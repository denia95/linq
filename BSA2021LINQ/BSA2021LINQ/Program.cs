﻿using BSA2021LINQ.Services;
using System;
using System.Linq;

namespace BSA2021LINQ
{
    class Program
    {
       private static void GetTaskNumber(int userId)
       {
            Service service = new Service();
            var projects = service.GetProjects();
            var tasks = service.GetTasks();
            var dictionary = projects
            .Where(p => p.AuthorId == userId)
            .ToDictionary(p => p, p => tasks.Count(t => t.ProjectId == p.Id));
       }

        private static void GetTaskList(int userId)
        {
            Service service = new Service();
            var tasks = service.GetTasks();
            var list = tasks
            .Where(t => t.PerformerId == userId && t.Name.Length < 45)
            .ToList();
        }

        private static void GetListTaskFromCollections(int userId)
        {
            Service service = new Service();
            var tasks = service.GetTasks();
            var list = tasks
            .Where(t => t.PerformerId == userId && t.FinishedAt.HasValue && t.FinishedAt.Value.Year == 2021)
            .Select(t => new { id = t.Id, name = t.Name })
            .ToList();
        }

        private static void GetListTeams()
        {
            
            Service service = new Service();
            var teams = service.GetTeams();
            var users = service.GetUsers();

            var list = users.Join(teams,
                u => u.TeamId,
                t => t.Id,
                (u, t) => new { Id = t.Id, Name = t.Name, User = u })
                .Where(u => DateTime.Now.Year - u.User.BirthDay.Year > 10)
                .OrderByDescending(u => u.User.RegisteredAt)
                .GroupBy(t => t.Id)
                .ToList();
        }

        private static void GetListUsers()
        {
            Service service = new Service();
            var tasks = service.GetTasks();
            var users = service.GetUsers();
            var list = users.Join(tasks,
                u => u.Id,
                t => t.PerformerId,
                (u, t) => new { User = u, Task = t })
                .OrderBy(u => u.User.FirstName)
                .ThenByDescending(t => t.Task.Name.Length)
                .ToList();
        }

        private static object GetProjectStruct(int projectId)
        {
            Service service = new Service();
            var users = service.GetUsers();
            var tasks = service.GetTasks();
            var project = service.GetProject(projectId);

            return new
            {
                Project = project,
                LongestTask = tasks.Where(t => t.ProjectId == project.Id).OrderByDescending(t => t.Description.Length).FirstOrDefault(),
                ShortestTask = tasks.Where(t => t.ProjectId == project.Id).OrderBy(t => t.Name.Length).FirstOrDefault(),
                UsersCount = users.Count(u => u.TeamId == project.TeamId && project.Description.Length > 20 || tasks.Count(t=> t.ProjectId == project.Id) < 3)
            };
        }

        private static object GetUserStruct(int userId)
        {
            Service service = new Service();
            var tasks = service.GetTasks();
            var projects = service.GetProjects(); 
            var user = service.GetUser(userId);

            return new {   
                User = user,
                LastProject = projects.Where(p => p.AuthorId == user.Id)
                    .OrderByDescending(p => p.CreatedAt)
                    .FirstOrDefault(),
                TasksCount = tasks.Count(t => t.ProjectId == projects.Where(p => p.AuthorId == user.Id).OrderByDescending(p => p.CreatedAt).FirstOrDefault()?.Id),
                NotFinishedTasksCount = tasks.Count(t => t.PerformerId == user.Id && t.State != 2),
                LongestTask = tasks.Where(t => t.PerformerId == user.Id).OrderByDescending(t => t.FinishedAt - t.CreatedAt).FirstOrDefault()};
        }


        static void Main(string[] args)
        {
           var a = GetProjectStruct(20);
        }
    }
}
