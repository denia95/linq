﻿using BSA2021LINQ.DTO;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;

namespace BSA2021LINQ.Services
{
    public class Service
    {
        private const string BASE_PATH = "https://bsa21.azurewebsites.net/api/";

        public List<ProjectDTO> GetProjects()
        {
            List<ProjectDTO> projects = new List<ProjectDTO>();
            string res;
            using (var client = new HttpClient())
            {
                var response = client.GetAsync(BASE_PATH + "Projects").Result;

                if (response.IsSuccessStatusCode)
                {
                    res = response.Content.ReadAsStringAsync().Result;
                    projects = JsonConvert.DeserializeObject<List<ProjectDTO>>(res);
                }
            }
            return projects;
        }

        public ProjectDTO GetProject(int id)
        {
            var project = new ProjectDTO();
            using (var client = new HttpClient())
            {
                var response = client.GetAsync(BASE_PATH + "Projects/" + id).Result;
                if (response.IsSuccessStatusCode)
                {
                    string json = response.Content.ReadAsStringAsync().Result;
                    project = JsonConvert.DeserializeObject<ProjectDTO>(json);
                }
            }
            return project;
        }

        public List<TaskDTO> GetTasks()
        {
            List<TaskDTO> tasks = new List<TaskDTO>();
            string res;
            using (var client = new HttpClient())
            {
                var response = client.GetAsync(BASE_PATH + "Tasks").Result;

                if (response.IsSuccessStatusCode)
                {
                    res = response.Content.ReadAsStringAsync().Result;
                    tasks = JsonConvert.DeserializeObject<List<TaskDTO>>(res);
                }
            }
            return tasks;
        }

        public TaskDTO GetTask(int id)
        {
            var task = new TaskDTO();
            using (var client = new HttpClient())
            {
                var response = client.GetAsync(BASE_PATH + "Tasks/" + id).Result;
                if (response.IsSuccessStatusCode)
                {
                    string json = response.Content.ReadAsStringAsync().Result;
                    task = JsonConvert.DeserializeObject<TaskDTO>(json);
                }
            }
            return task;
        }

        public List<TeamDTO> GetTeams()
        {
            List<TeamDTO> teams = new List<TeamDTO>();
            string res;
            using (var client = new HttpClient())
            {
                var response = client.GetAsync(BASE_PATH + "Teams").Result;

                if (response.IsSuccessStatusCode)
                {
                    res = response.Content.ReadAsStringAsync().Result;
                    teams = JsonConvert.DeserializeObject<List<TeamDTO>>(res);
                }
            }
            return teams;
        }

        public TeamDTO GetTeam(int id)
        {
            var team = new TeamDTO();
            using (var client = new HttpClient())
            {
                var response = client.GetAsync(BASE_PATH + "Teams/" + id).Result;
                if (response.IsSuccessStatusCode)
                {
                    string json = response.Content.ReadAsStringAsync().Result;
                    team = JsonConvert.DeserializeObject<TeamDTO>(json);
                }
            }
            return team;
        }

        public List<UserDTO> GetUsers()
        {
            List<UserDTO> users = new List<UserDTO>();
            string res;
            using (var client = new HttpClient())
            {
                var response = client.GetAsync(BASE_PATH + "Users").Result;

                if (response.IsSuccessStatusCode)
                {
                    res = response.Content.ReadAsStringAsync().Result;
                    users = JsonConvert.DeserializeObject<List<UserDTO>>(res);
                }
            }
            return users;
        }

        public UserDTO GetUser(int id)
        {
            var user = new UserDTO();
            using (var client = new HttpClient())
            {
                var response = client.GetAsync(BASE_PATH + "Users/" + id).Result;
                if (response.IsSuccessStatusCode)
                {
                    string json = response.Content.ReadAsStringAsync().Result;
                    user = JsonConvert.DeserializeObject<UserDTO>(json);
                }
            }
            return user;
        }
    }
}
